# app/__init__.py

from flask_restplus import Api
from flask import Blueprint

from .main.controller.cylinder_controller import api as cylinder_ns

blueprint = Blueprint('api', __name__, url_prefix='/api')

api = Api(blueprint,
            title = 'Flask RESTplus api with keycloak',
            version = '0.1',
            description = 'api for flask restplus web service',
            doc='/docs'
        )

api.add_namespace(cylinder_ns, path='/cylinders')