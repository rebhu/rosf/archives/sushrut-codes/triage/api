from flask import request
from flask_restplus import Resource, reqparse
from flask import app

# from ..util.wrappers import token_required
from ..util.dto import cylinderDto
from ..service.cylinder_service import register_cylinder, get_all_cylinders, get_cylinder, update_cylinder_status, list_cities, my_cylinder
from ..service.cylinder_request_service import add_cylinder_request, get_cylinder_request, get_request_number
from ..util.error_responses import *

api = cylinderDto.api
_cylinder = cylinderDto.cylinder
_cylinderUpdate = cylinderDto.cylinder_update
_cylinderRequest = cylinderDto.cylinder_request

# ----- REGISTER CYLINDER ---------- #
cylinder_parser1 = api.parser()
cylinder_parser1.add_argument('page', type=int, help="Page number for cylinders", required=True)
cylinder_parser1.add_argument('city', type=str, help="City name", required=True)

@api.route('', methods=['POST', 'GET'])
class registerCylinder(Resource):
    # define responses
    @api.response(200, "Cylinder registered")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Cylinder registration")
    @api.expect(_cylinder, validate=True)
    def post(self):
        """Register cylinder"""
        
        payload = request.json
        
        return register_cylinder(data=payload)

    # define responses
    @api.response(200, "Cylinders fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")
    @api.expect(cylinder_parser1)

    @api.doc("Cylinders fetched")
    def get(self):
        """Fetch cylinders"""
        
        args = cylinder_parser1.parse_args(strict=True)
        
        return get_all_cylinders(args)

# ---------------- MY CYLINDER ---------------
phone_parser = api.parser()
phone_parser.add_argument('phone', type=str, help="Phone number", required=True)

@api.route('/myCylinder', methods=['GET'])
class cylinderLogin(Resource):
    # define responses
    @api.response(200, "My Cylinder fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.expect(phone_parser)
    @api.doc("View my cylinder")
    def get(self):
        """My cylinder"""

        args = phone_parser.parse_args(strict=True)

        return my_cylinder(data=args)


# ------------- GET CITIES ----------------------
@api.route('/cities', methods=['GET'])
class getCities(Resource):
    # define responses
    @api.response(200, "City list fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("List Cities")
    def get(self):
        """List Cities"""

        return list_cities()


# ----- INDIVIDUAL CYLINDER OPERATIONS ---------- #
@api.route('/<cylinder_id>', methods=['GET', 'PUT'])
@api.doc(params={'cylinder_id': 'Cylinder ID'}, validate=True)
class cylinderState(Resource):

    # define responses --------------------[GET]
    @api.response(200, "Fetched cylinder")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Fetch cylinder")
    def get(self, cylinder_id):
        """Get cylinder"""
        
        return get_cylinder(data={'cylinder_id': cylinder_id})


    # define responses --------------------[PUT]
    @api.response(200, "Updated cylinder status")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")
    @api.expect(_cylinderUpdate, validate=True)
    @api.doc("update cylinder status")
    def put(self, cylinder_id):
        """Update cylinder status"""

        payload = request.json
        
        return update_cylinder_status(data={**payload, **{'cylinder_id': cylinder_id}})


# ===============================================================================================================
# ------------- REGISTER REQUESTS ----------------------
# ----- REQUEST OPERATIONS ---------- #
cylinder_parser2 = api.parser()
cylinder_parser2.add_argument('phone', type=str, help="Phone number", required=True)
cylinder_parser2.add_argument('passcode', type=str, help="Passcode", required=True)

@api.route('/<cylinder_id>/requests', methods=['POST', 'GET'])
@api.doc(params={'cylinder_id': 'Cylinder ID'}, validate=True)
class cylinderRequest(Resource):

    # define responses --------------------[POST]
    @api.response(200, "Cylinder requested")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")
    @api.expect(_cylinderRequest, validate=True)

    @api.doc("Add cylinder request")
    def post(self, cylinder_id):
        """Request cylinder"""

        payload = request.json
        return add_cylinder_request(data={**payload, **{'cylinder_id': cylinder_id}})
    
    # define responses --------------------[GET]
    @api.response(200, "Cylinder requests fetched")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")
    @api.expect(cylinder_parser2)

    @api.doc("Get cylinder requests")
    def get(self, cylinder_id):
        """Get cylinder requests"""

        args = cylinder_parser2.parse_args(strict=True)
        return get_cylinder_request(data={**args, **{'cylinder_id': cylinder_id}})

# ------------------ REQUEST NUMBERS ------------------
@api.route('/<cylinder_id>/requests/number', methods=['GET'])
@api.doc(params={'cylinder_id': 'Cylinder ID'}, validate=True)
class requestsNumber(Resource):
    @api.response(200, "Fetch requests number")
    @api.response(403, "Resource error")
    @api.response(406, "Validation error")

    @api.doc("Get number of requests")
    def get(self, cylinder_id):
        """Get number of requests"""

        return get_request_number(cylinder_id)