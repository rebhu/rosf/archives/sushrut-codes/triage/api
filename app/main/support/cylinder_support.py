import re
import datetime as dt
from hashlib import md5
import uuid

from app.main import db
from ..model.cylinder_model import Cylinder, CylinderRequest


def fetch_one_cylinder(cylinder_id, cylinder_keys):
    
    try:
        cylinder_details = Cylinder.query.filter_by(cylinder_id=db.func.UUID_TO_BIN(cylinder_id)).filter_by(status=1).first()
        
        if not cylinder_details:
            return False, 'No cylinder found'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message

    # create product dictionary
    cylinder_dict = cylinder_details.__dict__
    cylinder = {key: str(cylinder_dict[key]) for key in cylinder_keys}
    cylinder['cylinder_id'] = cylinder_id
    
    return True, cylinder


def fetch_cylinders(n, city, cylinder_keys):
    value = 10
    start_value = value * (int(n) - 1)
    
    try:
        all_cylinders = db.session.query(db.func.BIN_TO_UUID(Cylinder.cylinder_id), 
                                    Cylinder.owner_name, Cylinder.created_on, Cylinder.modified_on, 
                                    Cylinder.cylinder_type, Cylinder.quantity, Cylinder.condition, Cylinder.city, 
                                    Cylinder.pincode, Cylinder.state, Cylinder.phone, Cylinder.status).filter(Cylinder.status.like(1) & Cylinder.city.like(city)).order_by(db.desc(Cylinder.modified_on)).offset(start_value).limit(value).all()

    except Exception as err:

        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    
    all_dict = []
    for a_cylinder in all_cylinders:
        cylinder_dict = {key: str(a_cylinder[i]) for i, key in enumerate(cylinder_keys)}
        
        all_dict.append(cylinder_dict)

    
    return True, all_dict


def verify_owner(cylinder_id, phone, passcode):
    passcode_hash = md5(passcode.encode('utf-8')).hexdigest()
    try:
        cylinder_details = Cylinder.query.filter_by(cylinder_id=db.func.UUID_TO_BIN(cylinder_id)).first()
        
        if not cylinder_details:
            return False, 'No cylinder exist'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    
    db_phone = cylinder_details.phone
    db_passcode = cylinder_details.passcode

    if db_phone == phone and db_passcode == passcode_hash:
        return True, 'Owner verified'

    else:
        return False, 'Passcode or phone does not match'

def fetch_my_cylinder(phone, keys):
    try:
        cylinder_details = Cylinder.query.filter_by(phone=phone).filter_by(status=1).first()
        
        if not cylinder_details:
            return False, 'No cylinder exist'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    
    
    cylinder_dict = cylinder_details.__dict__
    my_cylinder = dict()
    for key in keys:
        if key == 'cylinder_id':
            my_cylinder[key] = str(B_U(cylinder_dict[key]))
        else: 
            my_cylinder[key] = str(cylinder_dict[key])
        
    return True, my_cylinder


def update_cylinder(cylinder_id):
    modified_on = dt.datetime.utcnow()
    try:
        cylinder_details = Cylinder.query.filter_by(cylinder_id=db.func.UUID_TO_BIN(cylinder_id)).update({'status': 0, 'modified_on': modified_on}, synchronize_session=False)
        
        db.session.commit()
        if not cylinder_details:
            return False, 'Wrong phone number'

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    
    return True, 'Cylinder deleted'

def fetch_cities():
    try:
        cities_list = Cylinder.query.with_entities(Cylinder.city).filter_by(status=1).distinct().all()
        
        if not cities_list:
            return False, "No entries found"
    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return False, message
    cities = [city[0] for city in cities_list]
    return True, cities

# support functions
def B_U(binary_uuid):
    return uuid.UUID(bytes=binary_uuid)