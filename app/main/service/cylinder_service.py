# service module for business related operations

import uuid
import datetime as dt
import re
from random import choice
from string import ascii_uppercase, digits
from hashlib import md5

from flask import current_app as app
from ..model.cylinder_model import Cylinder
from ..support.cylinder_support import fetch_cylinders, fetch_one_cylinder, verify_owner, update_cylinder, fetch_cities, fetch_my_cylinder
from ..util.validation_schema import validate_data, escape_char
from ..util.error_responses import *
from app.main import db
# ---------------------------------------------------------------------
#                           CYLINDER HANDLING
# ---------------------------------------------------------------------
def register_cylinder(data):

    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    
    # if validated then register --------------------------------------
    cylinder_id = str(uuid.uuid4())
    passcode = ''.join(choice(ascii_uppercase + digits) for _ in range(6))
    passcode_hash = md5(passcode.encode('utf-8')).hexdigest()
    
    try:
        cylinder_data = Cylinder(
                    owner_name = data['owner_name'],
                    cylinder_type = data['cylinder_type'],
                    quantity = data['quantity'],
                    condition = data['condition'],
                    city = data['city'],
                    pincode = int(data['pincode']),
                    state = data['state'],
                    created_on = dt.datetime.utcnow(),
                    modified_on = dt.datetime.utcnow(),
                    cylinder_id = db.func.UUID_TO_BIN(cylinder_id),
                    phone = data['phone'],
                    passcode = passcode_hash,
                    status = 1
                    )
        
        save_changes(cylinder_data)
        data['cylinder_id'] = cylinder_id
        data['passcode'] = passcode

        return data, 200

    except Exception as err:
        message = re.findall(r'\((.*?)\)', err.args[0])[-1]

        return error_response_403(message)

# -------------- FETCH MY CYLINDER ---------------------
def my_cylinder(data):
    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    cylinder_keys = ['cylinder_id', 'owner_name', 'created_on', 'modified_on', 
                    'cylinder_type', 'quantity', 'condition', 'city', 'pincode',
                    'state']
    cylinder_resp = fetch_my_cylinder(phone=data['phone'], keys=cylinder_keys)

    if not cylinder_resp[0]:
        return error_response_403(cylinder_resp[1])
    
    return cylinder_resp[1], 200

# -------------- FETCH CYLINDERS -----------------------
def get_all_cylinders(data):
    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    # define keys for cylinder data
    cylinder_keys = ['cylinder_id', 'owner_name', 'created_on', 'modified_on', 
                'cylinder_type', 'quantity', 'condition', 'city', 'pincode',
                'state']
    cylinder_resp = fetch_cylinders(n=data['page'], city=data['city'], cylinder_keys=cylinder_keys)

    if not cylinder_resp[0]:
        return error_response_403(cylinder_resp[1])

    return cylinder_resp[1], 200

# ------------- FETCH ONE CYLINDER ----------------------
def get_cylinder(data):
    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])

    # define keys for cylinder data
    cylinder_keys = ['owner_name', 'created_on', 'modified_on', 
                'cylinder_type', 'quantity', 'condition', 'city', 'pincode',
                'state']
    cylinder_resp = fetch_one_cylinder(data['cylinder_id'], cylinder_keys)

    if not cylinder_resp[0]:
        return error_response_403(cylinder_resp[1])

    return cylinder_resp[1], 200

# --------------- UPDATE CYLINDER STATUS ---------------
def update_cylinder_status(data):
    # escape characters
    data = escape_char(data)
    
    validated = validate_data(data, data.keys())
    if not validated[0]:
        return error_response_406(validated[1])
    
    # verify cylinder owner
    verification_response = verify_owner(cylinder_id=data['cylinder_id'], phone=data['phone'], passcode=data['passcode'])
    if not verification_response[0]:
        return error_response_403(verification_response[1])

    cylinder_resp = update_cylinder(cylinder_id=data['cylinder_id'])

    if not cylinder_resp[0]:
        return error_response_403(cylinder_resp[1])

    return cylinder_resp[1], 200

# ------------ FETCH ALL CITIES -------------------
def list_cities():
    cities_resp = fetch_cities()

    if not cities_resp[0]:
        return error_response_403(cities_resp[1])
    
    return cities_resp[1], 200


# support functions
def save_changes(data):
    db.session.add(data)
    db.session.commit()