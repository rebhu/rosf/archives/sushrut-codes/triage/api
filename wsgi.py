from app.main import create_app
from app import blueprint

application = create_app('dev')
application.register_blueprint(blueprint)

application.app_context().push()

if __name__ == "__main__":
    application.run(host="0.0.0.0", port=5000)